# Conda aspell-en-dict recipe

Works with the aspell package from conda forge and provides english variant
dictionaries.

## Create package
``` sh
conda build -c conda-forge .
```
