#!/bin/bash

set -ex

echo "SRC_DIR ${SRC_DIR}"

mkdir ${SRC_DIR}/aspell-dict-out
cd ${SRC_DIR}
./configure --vars DESTDIR=${SRC_DIR}/aspell-dict-out/ ASPELL_FLAGS="--dict-dir=./aspell-dict-out/"
make install
mkdir -p ${PREFIX}/lib/aspell-0.60
cp ${SRC_DIR}/aspell-dict-out/${PREFIX}/lib/aspell-0.60/* ${PREFIX}/lib/aspell-0.60
